function loadDAE(_dae){
  console.log("loading"+_dae);
  var loader = new THREE.ColladaLoader();
  loader.options.convertUpAxis = true;
  loader.load( _dae, function ( collada ) {
    dae = collada.scene;
    init();
    animate();
  });
}

function createScene(){
  container = document.createElement( 'div' );
  document.body.appendChild( container );
  camera = new THREE.PerspectiveCamera( 45, window.innerWidth / window.innerHeight, .1, 2000 );
  camera.position.set( 0,0,0 );

  scene = new THREE.Scene();

  renderer = new THREE.WebGLRenderer();
  renderer.setPixelRatio( window.devicePixelRatio );
  renderer.setSize( window.innerWidth, window.innerHeight );
  container.appendChild( renderer.domElement );

  controls = new THREE.FirstPersonControls(camera, renderer.domElement);
  controls.lookSpeed = 0.1;
  controls.movementSpeed = 10;
  controls.noFly = true;
  controls.lookVertical = false;
  controls.constrainVertical = true;
//  controls.verticalMin = 1.0;
//  controls.verticalMax = 2.0;
//  controls.lon = -150;
//  controls.lat = 120;
  //

  window.addEventListener( 'resize', onWindowResize, false );

}

function onWindowResize() {

  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();

  renderer.setSize( window.innerWidth, window.innerHeight );

}

